pub fn longest_incrementing_subslice(s: &[u8]) -> &[u8] {
    let mut i = 0;
    let mut j = 1;
    let mut icur = 0;
    let mut jcur = 1;
    let mut cur = s[0];
    for x in s[1..].iter() {
        if cur < 255 && *x == cur + 1 {
            cur = *x;
            jcur = jcur + 1;
        } else {
            if j - i < jcur - icur {
                i = icur;
                j = jcur;
            }
            icur = jcur;
            jcur = icur + 1;
            cur = s[icur];
        }
    }
    if j - i < jcur - icur {
        i = icur;
        j = jcur;
    }
    return &s[i..j];
}

fn main() {
    // let slice = [1, 2, 4, 4, 5, 6, 7, 3, 2, 7, 8, 9, 1];
    // let slice = [8, 1, 2, 3];
    let slice = [2, 5, 2, 253, 254, 255, 1];
    println!("{:?}", longest_incrementing_subslice(&slice));
}

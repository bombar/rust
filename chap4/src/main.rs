fn main() {
    let s = String::from("Ϫ");
    let size = calculate_length(&s);
    println!("La chaine '{}' a pour longueur '{}' ", &s[..], size);
}


fn calculate_length(s: &String) -> usize {
    s.len()
}

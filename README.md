# RUST

This is a simple repository with my toy examples following [official RUST tutorial](https://doc.rust-lang.org/book/title-page.html).


# Useful

 * Compiling with rustc

``` shell
rustc main.rs
```

 * Printing the assembly code

 ``` shell
 rustc --emit asm main.rs
 ```


 * Create a new repository
  ```shell
  cargo new <Repo>
  ```

  You may want to add ```--vcs=git``` to force the creation of a git repository.



 * Building the repo

 ```shell
 cargo build
 ```

 * Building and running the repo

 ```shell
 cargo run
 ```

 * Checking the code without producing a binary

 ```shell
     cargo check
 ```

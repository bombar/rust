fn main() {
    let x: f32 = 0.1 + 0.2 + 0.3;
    println!("The value of x is: {}", x);    
    let x: f32 = 0.1 + (0.2 + 0.3);
    println!("The value of x is: {}", x);
}

fn main() {
    let s2 = gives_ownership();

    let s2 = takes_and_gives_back(s2);

    println!("{}", s2);

    let s2 = String::from("剏");
    
    let size = calculate_length(&s2);
    println!("The size of {} is {} bytes.", s2, size);

    let index = first_space(&s2);

    if index == s2.len() {
        println!("There is no space in {}.", s2)
    } else {

        println!("The first space in {} is at index {}.", s2, index);
    }

    println!("On prend la slice [0..2] de {} : {}.", s2, &s2[0..2]);
}

fn first_space(s: &String) -> usize {

    let bytes = s.as_bytes();
    
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }

    s.len()
}

fn gives_ownership() -> String {
    let some_string = String::from("Hello");

    some_string
}


fn takes_and_gives_back(a_string: String) -> String {
    println!("{}", a_string);
    String::from("World !")
}

fn calculate_length(s: &String) -> usize {
    s.len()
}
